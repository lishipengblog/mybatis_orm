package com.lspshoping.session;

public interface SqlSessionFactory {
    SqlSession openSession();
}
