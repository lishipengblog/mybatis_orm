package com.lspshoping.dao;

import com.lspshoping.annotations.Select;
import com.lspshoping.domain.User;

import java.util.List;

public interface IUserMapper {
    @Select("select * from user")
    List<User> findAll();

    @Select("select * from user where username = ?")
    List<User> findUserName(String username);
}
