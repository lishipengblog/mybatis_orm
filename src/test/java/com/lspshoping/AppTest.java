package com.lspshoping;


import com.lspshoping.dao.IUserMapper;
import com.lspshoping.domain.User;
import com.lspshoping.io.Resources;
import com.lspshoping.session.SqlSession;
import com.lspshoping.session.SqlSessionFactory;
import com.lspshoping.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        //1.dom4j 获取配置文件输入流
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        //2.dom4j读取配置文件，数据库链接信息
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        //3.与数据库建立链接
        SqlSession sqlSession = factory.openSession();
        //4.获取数据库返回类型与实体
        IUserMapper mapper = sqlSession.getMapper(IUserMapper.class);
        //5.根据获取到数据库的链接，与返回类型，执行特定的方法，赋值给对应的字段
//        List<User> users = mapper.findAll();
        List<User> users = mapper.findUserName("李仕鹏");

        //6.遍历输出返回的内容
        for(User user: users){
            System.out.println(user);
        }
        //7.关闭会话
        sqlSession.close();
        //8.关闭流
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Test
    public void fileTest(){
        URL url = AppTest.class.getClassLoader().getResource("mapper");
        assert url != null;
        File file = new File(url.getFile());
        File[] files = file.listFiles();
        for (File file1 : files){
            System.out.println(file1);
        }
    }
}
