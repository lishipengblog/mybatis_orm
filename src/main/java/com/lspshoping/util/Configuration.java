package com.lspshoping.util;

import java.util.HashMap;
import java.util.Map;

public class Configuration {

    private boolean mapUnderscoreToCamelCase;

    private String driver;

    private String url;

    private String password;

    private String username;

    public boolean isMapUnderscoreToCamelCase() {
        return mapUnderscoreToCamelCase;
    }

    public void setMapUnderscoreToCamelCase(boolean mapUnderscoreToCamelCase) {
        this.mapUnderscoreToCamelCase = mapUnderscoreToCamelCase;
    }

    private Map<String, SqlMapper> sqlMappers = new HashMap<String, SqlMapper>();

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Map<String, SqlMapper> getSqlMappers() {
        return sqlMappers;
    }

    public void setSqlMappers(Map<String, SqlMapper> sqlMappers) {
        this.sqlMappers.putAll(sqlMappers);
    }
}
