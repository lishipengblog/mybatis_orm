package com.lspshoping.session;

import com.lspshoping.session.defaults.DefaultSqlSessionFactory;
import com.lspshoping.util.XMLConfigBuilder;

import java.io.InputStream;

/**
 * 根据输入输出流构建SqlSessionFactoryBuilder对象
 */
public class SqlSessionFactoryBuilder {
    public SqlSessionFactory build(InputStream inputStream) {
        return new DefaultSqlSessionFactory(XMLConfigBuilder.loadConfiguration(inputStream));
    }
}
