package com.lspshoping.binding;

import com.lspshoping.util.Executor;
import com.lspshoping.util.SqlMapper;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.Map;

public class MapperProxy implements InvocationHandler {

    private Map<String, SqlMapper> sqlMappers;

    private Connection connection;

    public MapperProxy(Map<String, SqlMapper> sqlMappers, Connection connection) {
        this.sqlMappers = sqlMappers;
        this.connection = connection;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        String className = method.getDeclaringClass().getName();
        SqlMapper sqlMapper = sqlMappers.get(className + "." + methodName);
        return new Executor().selectList(sqlMapper, connection, args);
    }
}
