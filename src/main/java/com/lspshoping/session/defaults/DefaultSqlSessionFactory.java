package com.lspshoping.session.defaults;

import com.lspshoping.session.SqlSession;
import com.lspshoping.session.SqlSessionFactory;
import com.lspshoping.util.Configuration;
import com.lspshoping.util.DataSourceUtil;

public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration.getSqlMappers(), DataSourceUtil.getConnnection(configuration));
    }
}
